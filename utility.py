import numpy as np
from matplotlib import pyplot
from scipy.optimize import fmin
from ase.parallel import paropen
from time import time

__author__ = 'Andrew Peterson, Cheng Zeng'


def makefig(figsize=(5., 5.), nh=1, nv=1,
            lm=0.1, rm=0.1, bm=0.1, tm=0.1, hg=0.1, vg=0.1,
            hr=None, vr=None):
    """
    figsize: canvas size
    nh, nv: number of horizontal and vertical images
    lm, rm, bm, tm, hg, vg: margins and "gaps"
    hr is horizontal ratio, and can be fed in as for example
    (1., 2.) to make the second axis twice as wide as the first.
    [same for vr]
    vg can also be fed as an array of length equivalent to the number
      of vertical gaps
    """
    hr = np.array(hr, dtype=float) / np.sum(hr) if hr else np.ones(nh) / nh
    vr = np.array(vr, dtype=float) / np.sum(vr) if vr else np.ones(nv) / nv

    vg = vg * np.ones(nv - 1) if type(vg) is float else np.array(vg)
    vg = np.append(vg, 0.)  # Dummy value to make counter iv work below.

    axwidths = (1. - lm - rm - (nh - 1.) * hg) * hr
    axheights = (1. - bm - tm - sum(vg)) * vr

    fig = pyplot.figure(figsize=figsize)
    axes = []
    leftpoint = lm
    for ih, h in enumerate(range(nh)):
        bottompoint = 1. - tm
        for iv, v in enumerate(range(nv)):
            bottompoint -= axheights[iv]
            ax = fig.add_axes((leftpoint, bottompoint,
                               axwidths[ih], axheights[iv]))
            axes.append(ax)
            bottompoint -= vg[iv]
        leftpoint += hg + axwidths[ih]

    return fig, axes


def make_parity_plot(xs, ys, labels=False, legend=None, markersize=None,
                     offset=True, save_plot=False, plot_name=False,
                     title=False):
    """
    Make customized parity plot.
    """
    xs, ys = np.array(xs), np.array(ys)
    rmse = np.sqrt(np.mean((xs-ys)**2))
    assert len(xs) == len(ys), "Check length of xs and ys."
    fig, _ax = pyplot.subplots(figsize=(4, 4), dpi=300)
    _ax.autoscale(tight=True)
    _ax.set_aspect('equal')
    _ax.plot(xs, ys, 'r.',
             markersize=14 if markersize is None else markersize,
             label=legend)
    xlim = _ax.get_xlim()
    ylim = _ax.get_ylim()
    maxlim = np.max([xlim, ylim])
    minlim = np.min([xlim, ylim])
    lims = np.array([minlim, maxlim])
    _ax.plot(lims, lims, '-', color='0.5', zorder=1)
    if not offset:
        return
    else:
        offset = offset if isinstance(offset, float) else rmse
        _ax.plot(lims, lims+offset, '--', color='0.5', zorder=1)
        _ax.plot(lims, lims-offset, '--', color='0.5', zorder=1)
        _ax.fill_between(lims, lims-offset, lims+offset,
                         facecolor='grey', alpha=0.3,)
    _ax.set_xlim(lims)
    _ax.set_ylim(lims)
    _ax.tick_params(direction='in')
    for tick in _ax.xaxis.get_major_ticks():
        tick.label.set_fontsize(10)
    for tick in _ax.yaxis.get_major_ticks():
        tick.label.set_fontsize(10)
    pyplot.text(0.90, 0.1, r'$RMSD$: {:.2f}'.format(rmse),
                verticalalignment='bottom', horizontalalignment='right',
                transform=pyplot.gca().transAxes,
                color='k', fontsize=14)
    if labels:
        [xs, ys] = labels.values() if isinstance(labels, dict) else labels
        _ax.set_xlabel(xs, fontsize=10)
        _ax.set_ylabel(ys, fontsize=10)
    if title:
        _ax.set_title(title)
    if save_plot:
        if not plot_name:
            fig.savefig('parity_plot.pdf')
        else:
            assert isinstance(plot_name, str)
            fig.savefig("%s" % plot_name)
    if legend:
        _ax.legend()


class PolyRegress(object):
    """
    Simple user speficied function fitting with zero intercept. Note
    that the default function to be fitted is a linear function with
    only two parameters. However, you can input customized function
    with the argument `func` to define the function you want.

    Inspired by Andrew Peterson's script
    """

    def __init__(self, xs, ys, linear=True, func=None):
        assert len(list(xs)) == len(list(ys)), "Check length of xs and ys."
        self.xs = np.array(xs)
        self.ys = np.array(ys)
        self.linear = linear
        self.func = func

    def _model(self, x, parameters):
        if self.linear and self.func is None:
            return parameters[0] * x + parameters[1]
        elif self.func is not None:
            return self.func(x, parameters)

    def _ssr(self, parameters):
        ypreds = self._model(self.xs, parameters)
        square_resids = (self.ys - ypreds)**2
        return sum(square_resids)

    def get_fit_answer(self,):
        xs, ys = self.xs, self.ys
        answer = fmin(self._ssr, x0=[0., 0.])
        if self.linear:
            r_squared = np.sum((answer[0]*xs + answer[1] - np.mean(ys))**2)\
                        / (np.sum((ys-np.mean(ys))**2))
            return np.round(answer, 3), np.round(r_squared, 3)
        else:
            return np.round(answer, 3)


# Logger is copied from Andy's code.
class Logger:

    """Logger that can also deliver timing information.

    Parameters
    ----------
    file : str
        File object or path to the file to write to.  Or set to None for
        a logger that does nothing.
    """

    def __init__(self, file):
        if file is None:
            self.file = None
            return
        if isinstance(file, str):
            self.filename = file
            self.file = paropen(file, 'a')
        self.tics = {}

    def tic(self, label=None):
        """Start a timer.

        Parameters
        ----------
        label : str
            Label for managing multiple timers.
        """
        if self.file is None:
            return
        if label:
            self.tics[label] = time.time()
        else:
            self._tic = time.time()

    def __call__(self, message, toc=None, tic=False):
        """Writes message to the log file.

        Parameters
        ---------
        message : str
            Message to be written.
        toc : bool or str
            If toc=True or toc=label, it will append timing information in
            minutes to the timer.
        tic : bool or str
            If tic=True or tic=label, will start the generic timer or a timer
            associated with label. Equivalent to self.tic(label).
        """
        if self.file is None:
            return
        dt = ''
        if toc:
            if toc is True:
                tic = self._tic
            else:
                tic = self.tics[toc]
            dt = (time.time() - tic) / 60.
            dt = ' %.1f min.' % dt
        if self.file.closed:
            self.file = open(self.filename, 'a')
        self.file.write(message + dt + '\n')
        self.file.flush()
        if tic:
            if tic is True:
                self.tic()
            else:
                self.tic(label=tic)
